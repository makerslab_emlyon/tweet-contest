const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const twit = require('twit');

let config;
try {
    config = require('./env.json');
} catch (e) {
    console.log(e);
    config = process.env;
}

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.post('/git', (request, response) => {
    if (request.headers['x-gitlab-event'] === 'Push Hook' && request.headers['x-gitlab-token'] === process.env.GIT) {
        response.sendStatus(200);

        const { execSync } = require('child_process');
        const commands = [
            'git fetch origin master',
            'git reset --hard origin/master',
            'git pull origin master --force',
            'npm install',
            'refresh' // refresh glitch UI
        ];
        for (const cmd of commands) {
            console.log(execSync(cmd).toString());
        }
        console.log('Successfully updated!');
        return;
    }
    else {
        console.log('Webhook signature incorrect!');
        return response.sendStatus(403);
    }
});

const listener = http.listen(process.env.PORT || 3000, () => {
    console.log(`Your app is listening on port ${listener.address().port}`);

    searchTweets(query).catch(err => {
        console.log(err);
    });
});

const Twitter = new twit({
    ...config,
    timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
    strictSSL: true, // optional - requires SSL certificates to be valid.
});

const query = '#uxemlyon';
const hashtag1 = 'paris';
const hashtag2 = 'sainté';

const oldest = 1171069636902825985;

const tweets = [[], []];
const banned = [];
let total = 0;

io.on('connection', socket => {
    socket.emit('data', tweets);

    socket.on('remove', data => {
        const { pass, id } = data;

        if (pass === config.PASS) {
            banned.push(id);

            tweets[0] = tweets[0].filter(t => !banned.includes(t.id));
            tweets[1] = tweets[1].filter(t => !banned.includes(t.id));

            socket.emit('data', tweets);
        } else {
            socket.emit('wrong-pass');
        }
    });
});

function searchTweets(q, max_id, loop = true) {
    console.log(Date.now(), `Searching tweets${max_id ? ` with max_id ${max_id}` : ''}`);

    return new Promise((resolve, reject) => {
        Twitter.get('search/tweets', {
            q,
            max_id,
            count: 100,
            result_type: 'recent',
            tweet_mode: 'extended'
        }).then(result => {
            // Save tweets
            for (const tweet of result.data.statuses) {
                // Ignore banned tweets
                if (banned.includes(tweet.id)) continue;

                // Ignore RTs
                if (tweet.retweeted_status) continue;

                // Hashtag 1
                if (tweet.entities.hashtags.some(hashtag => hashtag.text.toLowerCase() === hashtag1)) {
                    if (!tweets[0].some(t => t.id === tweet.id)) tweets[0].push(tweet);
                }

                // Hashtag 2
                if (tweet.entities.hashtags.some(hashtag => hashtag.text.toLowerCase() === hashtag2)) {
                    if (!tweets[1].some(t => t.id === tweet.id)) tweets[1].push(tweet);
                }
            }

            // Emit tweets (if there are new tweets)
            const newTotal = tweets[0].length + tweets[1].length;

            if (total !== newTotal) {
                total = newTotal;
                io.emit('data', tweets);
            }

            // Get max_id for next search (older results)
            const max_id = new URLSearchParams(result.data.search_metadata.next_results).get('max_id');

            if (loop) {
                if (result.data.statuses.length > 0 && max_id && max_id > oldest) {
                    // Reroll search
                    searchTweets(query, max_id);
                } else {
                    // Start listening for new tweets
                    setInterval(() => {
                        searchTweets(query, undefined, false);
                    }, 1000 * 10);
                }
            }
        }).catch(err => {
            reject(err.stack);
        });
    });
}
