const socket = io();

const deadline = 1568299500000;

$(document).ready(() => {
    updateTimer();

    setInterval(() => {
        updateTimer();
    }, 1000);
});

function updateTimer() {
    const now = Date.now();
    const delta = Math.max(deadline - now, 0);
    const h = Math.floor(delta / 1000 / 3600).toString().padStart(2, '0');
    const m = (Math.floor(delta / 1000 / 60) % 60).toString().padStart(2, '0');
    const s = (Math.floor(delta / 1000) % 60).toString().padStart(2, '0');
    $('.timer p').html(`${h}:${m}:${s}`);
}

socket.on('data', data => {
    $('.tweet').remove();
    $('#popup-wrap').hide();

    for (const col in data) {
        // console.log(data[col]);
        data[col] = data[col].sort((a, b) => new Date(a.created_at) - new Date(b.created_at));

        if (!data[col].length) $('.col').eq(col).find('h2').after(`<div class="tweet none"><p>No tweet yet 😞</p></div>`);

        for (const tweet of data[col]) {
            // console.log(tweet);
            let html = '';

            let text = parseTweet(tweet).text;
            let img = parseTweet(tweet).img;

            let quote = '';
            if (tweet.quoted_status) {
                html = createTweetHTML(tweet, text, img, tweet.quoted_status);
            } else {
                html = createTweetHTML(tweet, text, img);
            }

            $('.col').eq(col).find('h2').after(html);
        }
    }

    $('.score').html(`${data[0].length + 40} — ${data[1].length}`)
});

function parseTweet(tweet) {
    let text = tweet.full_text;
    text = text.replace(/https:\/\/t\.co\/\w+/g, '');

    for (const url of tweet.entities.urls) {
        text = text.replace(url.url, `<a href="${url.url}" target="_blank">${url.display_url}</a>`);
    }

    text = text.replace(/\n/g, '\<br\>');
    text = text.replace(/(#([A-zÀ-ÿ]+\d*)+)/g, `<a href="https://twitter.com/hashtag/$2" target="_blank">$1</a>`);
    text = text.replace(/@(\w+)/g, `<a href="https://twitter.com/$1" target="_blank">@$1</a>`);

    let img = '';

    if (tweet.entities.media) {
        img += '<div class="img">'
        for (const media of tweet.entities.media) {
            img += `<div style="background-image: url(${media.media_url})"></div>`;
        }
        img += '</div>';
    }

    return { text, img };
}

function createTweetHTML(tweet, text, img, quote) {
    return `<div class="tweet" data-id="${tweet.id}">
        <div class="remove">✕</div>
        <div class="user">
            <img src="${tweet.user.profile_image_url}">
            <div class="meta">
                <h3><a href="${`https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`}" target="_blank">${tweet.user.name}</a></h3>
                <h4>@${tweet.user.screen_name}</h4>
            </div>
        </div>
        <div class="content">
            <p>${text}</p>
            ${img ? img : ''}
            ${quote ? `<div class="tweet quote">
                <div class="user">
                    <img src="${quote.user.profile_image_url}">
                    <div class="meta">
                        <h3><a href="${`https://twitter.com/${quote.user.screen_name}/status/${quote.id_str}`}" target="_blank">${quote.user.name}</a></h3>
                        <h4>@${quote.user.screen_name}</h4>
                    </div>
                </div>
                <div class="content">
                    <p>${parseTweet(quote).text}</p>
                </div>
            </div>` : ''}
        </div>
    </a>`;
}

$(document).on('click', '.tweet .remove', e => {
    const id = parseInt($(e.target).closest('.tweet').attr('data-id'));
    const $tweet = $(`.tweet[data-id="${id}"]`);

    $tweet.hide();
    $('#popup-wrap').show();

    $('#popup form').on('submit', () => {
        socket.emit('remove', {
            pass: $('#popup form input').val(),
            id
        });
    });
});

socket.on('wrong-pass', () => {
    $('#popup').css('animation', 'wiggle .5s ease');
    setTimeout(() => {
        $('#popup').css('animation', '');
    }, 500);
});
